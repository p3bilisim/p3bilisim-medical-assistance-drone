package com.fa.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="Ateslenme.db";//Veritabanı Adı
    private static final String DB_TABLE="Ateslenme_Table";//Tablo Adı

    //Sütunlar
    private static final String Id="Id";
    private static final String ateslenme_zamani="ateslenme_zamani";

    //Create table query si
    private static final String CREATE_TABLE="CREATE TABLE "+ DB_TABLE+" ("+
            Id + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            ateslenme_zamani+ " TEXT "+ ")";

    //Constructor
    public DatabaseHelper(Context context)
    {
        super(context,DB_NAME,null,1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);//Çalıştırıldığında tablo oluştur komutu
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ DB_TABLE);//Değişiklik durumunda tabloyu yeniden oluştur komutu
        onCreate(sqLiteDatabase);
    }

    //veri girişi
    public boolean insertData(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(ateslenme_zamani,name);
        long result = db.insert(DB_TABLE,null,contentValues);
        return result !=-1;
    }

    //veri okuma
    public Cursor viewData(){
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT * FROM "+DB_TABLE;
        Cursor cursor=db.rawQuery(query,null);
        return cursor;
    }

    //veri silme
    public Integer deleteData(String column_name,String thingToDelete){
        SQLiteDatabase db= this.getWritableDatabase();
        return db.delete(DB_TABLE,null,null);
    }

    //son 30 gün verisi okuma
    public Cursor get30Data(){
        SQLiteDatabase db=this.getWritableDatabase();
        String query="SELECT strftime('%Y-%m-%d',"+ateslenme_zamani+") FROM "+DB_TABLE+" WHERE "+ateslenme_zamani+" >= datetime('now','localtime','start of day','-29 day')";
        Cursor cursor=db.rawQuery(query,null);
        return cursor;
    }
}
