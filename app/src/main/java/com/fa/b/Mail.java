package com.fa.b;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail extends AsyncTask<Void,Void,Void> {

    private Context context;//İçerik tanımı
    private Session session;
    private String email="info@compositeware.com", subject="İHBAR";
    public String message="İhbar";//Gönderilecek email tanımı
    private ProgressDialog progressDialog;//Uyarı ekranı tanımı


    public Mail(Context context){
        this.context=context;
    }

    //Mail gönderilirken arkaplanda çalışacak işlemler
    @Override
    protected Void doInBackground(Void... params) {
        Properties props=new Properties();

        //Smtp sunucu bilgileri
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "in-v3.mailjet.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.quitwait", "false");

        //Smtp sunucu kullanıcı şifre bilgisi
        session=Session.getDefaultInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("89fb05c328418821f8c261b13b4de697","d22e7a81f5eb48947710bce04e426507");
            }
        });

        //Mail gönderme komutları
        try{
            MimeMessage mimeMessage=new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress("info@compositeware.com"));
            mimeMessage.addRecipient(Message.RecipientType.TO,new InternetAddress(email));
            mimeMessage.setSubject(subject);
            mimeMessage.setText(message);
            Transport.send(mimeMessage);

        }

        //Exception yakalama
        catch (MessagingException e){
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    //İşlem tamamlandığındaki uyarı ekranı
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        Toast.makeText(context,"İhbar gönderildi",Toast.LENGTH_LONG).show();
    }

    //İşlem başlangıcındaki uyarı ekranı
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog=progressDialog.show(context,"İhbar Gönderiliyor","Lütfen Bekleyin...",false,false);

    }
}
