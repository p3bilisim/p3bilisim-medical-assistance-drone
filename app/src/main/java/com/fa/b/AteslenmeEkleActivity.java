package com.fa.b;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.Calendar;
import android.widget.DatePicker;
import android.widget.Toast;
import com.ikovac.timepickerwithseconds.TimePicker;
import com.ikovac.timepickerwithseconds.MyTimePickerDialog;

public class AteslenmeEkleActivity extends AppCompatActivity {

    DatabaseHelper db;//Database İşlem sınıfı tanımı
    Button btnDatePicker, btnTimePicker,kaydet; //Buton tanımı
    EditText txtDate, txtTime; //Saat ve tarih yazılan yerlerin tanımı
    private int mYear, mMonth, mDay, mHour, mMinute,mSecond;// Saat ve Tarihe dayalı verilerin tutulduğu değişkenler
    private String birlesim;//Tarih ve saatin birleştirilip veritabanında tutulacak hale getirileceği değişken

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ateslenme_ekle);

        //Database İşlem sınıfı tanımı
        db=new DatabaseHelper(this);

        //Itemlerin tanımlanması
        btnDatePicker=findViewById(R.id.btn_date);
        btnTimePicker=findViewById(R.id.btn_time);
        txtDate=findViewById(R.id.in_date);
        txtTime=findViewById(R.id.in_time);
        kaydet=findViewById(R.id.kaydet);
        Toolbar toolbar=findViewById(R.id.toolbar);

        //Elle veri girişi kapatma
        txtTime.setEnabled(false);
        txtDate.setEnabled(false);

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);

    }

    //Toolbardaki seçenekler tuşuna basılınca menü çıkması
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }

    //Toolbar menüsünde seçim yapılınca çalışacak kodlar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            //anasayfa seçilince anasayfa activity açılması
            case R.id.anasayfamenu:
                startActivity(new Intent(AteslenmeEkleActivity.this, AnasayfaActivity.class));
                return true;


            //işlemler seçilince işlemler activity açılması
            case R.id.islemlermenu:
                startActivity(new Intent(AteslenmeEkleActivity.this, IslemlerActivity.class));
                return true;


            //fabeacon seçilince fabeacon activity açılması
            case R.id.fabeaconmenu:
                startActivity(new Intent(AteslenmeEkleActivity.this, FABeaconActivity.class));
                return true;


            //ihbar seçilince ihbar butonunun bulunduğu anasayfa activity açılması
            case R.id.ihbarmenu:
                startActivity(new Intent(AteslenmeEkleActivity.this, AnasayfaActivity.class));
                return true;


            //hakkında seçilince hakkında activity açılması
            case R.id.hakkinda:
                startActivity(new Intent(AteslenmeEkleActivity.this, HakkindaActivity.class));
                return true;
        }
        return false;
    }

    //Tarihi seç butonu fonksiyonu
    public void btnDatePicker(View view)
    {
        // Şimdiki Zamanı Al
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);//Yıl bilgisi
        mMonth = c.get(Calendar.MONTH);//Ay bilgisi
        mDay = c.get(Calendar.DAY_OF_MONTH);//Gün bilgisi

        //Tarih seçmek için takvim açılması
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        //Bilgiler alındıktan sonra format ayarlama
                        txtDate.setText(year+
                                "-" + String.format("%02d", (monthOfYear + 1)) +
                                "-" + String.format("%02d", dayOfMonth));
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    //Saat seç butonu fonksiyonu
    public void btnTimePicker(View view)
    {
        // Şimdiki Zamanı Al
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);//Saat bilgisi
        mMinute = c.get(Calendar.MINUTE);//Dakika bilgisi
        mSecond=c.get(Calendar.SECOND);//Saniye bilgisi

        //Saat seçmek için ekran açılması
        MyTimePickerDialog mTimePicker = new MyTimePickerDialog(this, new MyTimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute, int seconds) {
                //Bilgiler alındıktan sonra format ayarlama
                txtTime.setText(String.format("%02d", hourOfDay)+
                        ":" + String.format("%02d", minute) +
                        ":" + String.format("%02d", seconds));
            }
        }, mHour, mMinute, mSecond, true);
        mTimePicker.show();
    }

    //Kaydet butonu fonksiyonu
    public void kaydet(View view){
        birlesim=""+txtDate.getText().toString()+" "+txtTime.getText().toString()+"";
        //Boş alan kontrolü
        if(txtDate.getText().toString().equals("")||txtTime.getText().toString().equals(""))
        {
            Toast.makeText(AteslenmeEkleActivity.this,"Boş Alan Bırakmayın",Toast.LENGTH_SHORT).show();
        }
        else{
            //Kayıt başarılı mı kontrolü
            if(db.insertData(birlesim)){
                AteslenmeEkleActivity.this.finish();
                Toast.makeText(AteslenmeEkleActivity.this,"Veri Eklendi",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(AteslenmeEkleActivity.this, AteslenmeZamanlariActivity.class));

            }
            else{
                Toast.makeText(AteslenmeEkleActivity.this,"Veri Eklenemedi",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
