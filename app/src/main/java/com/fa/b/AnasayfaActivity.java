package com.fa.b;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.net.NetworkInterface;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class AnasayfaActivity extends AppCompatActivity {


    //Activity oluşturulduğunda çalışacak komutlar
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anasayfa);

        //activity_anasayfa da ki itemlerin tanımlanması
        Toolbar toolbar=findViewById(R.id.toolbar);
        Button islemler_buton=findViewById(R.id.islemler_buton);
        Button fabeacon_buton=findViewById(R.id.fabeacon_buton);
        Button ihbar_buton=findViewById(R.id.ihbar_buton);

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);
    }


    //Toolbardaki seçenekler tuşuna basılınca menü çıkması
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }


    //Toolbar menüsünde seçim yapılınca çalışacak kodlar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            //işlemler seçilince işlemler activity açılması
            case R.id.islemlermenu:
                startActivity(new Intent(AnasayfaActivity.this, IslemlerActivity.class));
                return true;


            //fabeacon seçilince fabeacon activity açılması
            case R.id.fabeaconmenu:
                startActivity(new Intent(AnasayfaActivity.this, FABeaconActivity.class));
                return true;


            //hakkında seçilince hakkında activity açılması
            case R.id.hakkinda:
                startActivity(new Intent(AnasayfaActivity.this, HakkindaActivity.class));
                return true;
        }
        return false;
    }

    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }

    //işlemler butonu seçilince işlemler activity açılması
    public void islemler_buton(View view)
    {
        startActivity(new Intent(AnasayfaActivity.this, IslemlerActivity.class));
    }


    //fabeacon butonu seçilince fabeacon activity açılması
    public void fabeacon_buton(View view)
    {
        startActivity(new Intent(AnasayfaActivity.this, FABeaconActivity.class));
    }


    //ihbar butonu seçilince ihbar activity açılması
    public void ihbar_buton(View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(AnasayfaActivity.this);
        builder.setCancelable(true);
        builder.setTitle("İHBAR!");
        builder.setMessage("İhbarda bulunmmak istediğinize emin misiniz?");
        builder.setPositiveButton("İhbarda Bulun",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendEmail();
                        query2();
                    }
                });
        builder.setNegativeButton("İptal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }


    //Email göndermek için gereken class çağrılması
    public void sendEmail(){
        Mail mail =new Mail(this);
        mail.message="İhbar gelen cihazın mac adresi: "+getMacAddr();
        mail.execute();
    }

    public static String getMacAddr() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "";
    }

    public Connection connectionclass() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection connection=null;
        String ConnectionURL=null;
        try{
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL="jdbc:jtds:sqlserver://bakas.database.windows.net:1433;DatabaseName=Bakas_database;user=bakasbilisim@bakas;password=bakas.123;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
            connection= DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se){
        }
        catch (ClassNotFoundException e){}
        catch (Exception e){}
        return connection;
    }

    public void query2()
    {
        try{
            Connection con=connectionclass();
            if(con!=null) {
                //String query="select * from ihbar";
                String query="insert into ihbar (mac_adresi,ihbar_zamani) values ('"+getMacAddr()+"','"+getCurrentTimeStamp()+"')";
                Statement stmt=con.createStatement();
                if(!stmt.execute(query)){
                    Toast.makeText(this,"Başarısız",Toast.LENGTH_SHORT);
                }
                con.close();
            }
            else{
                Toast.makeText(this,"İnternet Bağlantınızı Kontrol Ediniz",Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception ex){

        }
    }


    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }


}
