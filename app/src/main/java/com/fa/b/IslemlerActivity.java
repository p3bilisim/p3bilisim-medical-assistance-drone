

package com.fa.b;

import androidx.appcompat.app.AppCompatActivity;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class IslemlerActivity extends AppCompatActivity {


    // TAG bilgi mesajları için kullanılır
    private final static String TAG = IslemlerActivity.class.getSimpleName();

    // Düğmeler, anahtarlar, değerler gibi mizanpajdaki nesnelere erişmek için değişkenler
    private static TextView mCapsenseValue;
    private static Switch start_button;
    private static TextView search_button1;
    private static Button search_button2;
    private static TextView search_button3;
    private static TextView connect_button1;
    private static Button connect_button2;
    private static TextView connect_button3;
    private static TextView discover_button1;
    private static Button discover_button2;
    private static TextView discover_button3;
    private static TextView disconnect_button1;
    private static Button disconnect_button2;
    private static TextView disconnect_button3;
    private static Switch led_switch;
    private static Switch cap_switch;
    private static Switch button0;
    private static Switch led_switch2;

    // BLE bağlantısını yönetmek için değişkenler
    private static boolean mConnectState;
    private static boolean mServiceConnected;
    private static PSoCCapSenseLedService mPSoCCapSenseLedService;
    //private static PSoCCapSenseButtonService mPSoCCapSenseButtonService;

    private static final int REQUEST_ENABLE_BLE = 1;

    //Bu, Android 6.0 (Marshmallow) için gereklidir
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    //CapSense Bildirimlerinin açık veya kapalı olup olmadığını takip edin
    private static boolean CapSenseNotifyState = false;
    private static boolean BUTTONDURUM = false;

    /**
     * Bu, BLE hizmetini yönetir.
     * Hizmet başladığında, hizmet nesnesini alır ve hizmeti başlatırız.
     */
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        /**
         * Bu, PSoCCapSenseLedService bağlandığında çağrılır
         *
         * @param componentName bağlı olan hizmetin bileşen adı
         * @param service bağlı servis
         */
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Log.i(TAG, "onServiceConnected");
            mPSoCCapSenseLedService = ((PSoCCapSenseLedService.LocalBinder) service).getService();
            mServiceConnected = true;
            mPSoCCapSenseLedService.initialize();
        }

        /**
         * Burası, PSoCCapSenseService bağlantısı kesildiğinde çağrılır.
         *
         * @param componentName bağlı olan hizmetin bileşen adı
         */
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.i(TAG, "onServiceDisconnected");
            mPSoCCapSenseLedService = null;


        }
    };

    /**
     * Ana aktivite ilk oluşturulduğunda buna denir
     *
     * @param savedInstanceState Intance durumu korundu
     *
     */
    @TargetApi(Build.VERSION_CODES.M) // This is required for Android 6.0 (Marshmallow) to work
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_islemler);

        // Ekrandaki CapSense değerini gösterecek bir değişken ayarlayın
        mCapsenseValue = (TextView) findViewById(R.id.capsense_value);

        // Düğmelere ve slayt anahtarlarına erişmek için değişkenleri ayarlama
        start_button = (Switch) findViewById(R.id.start_button);
        search_button1 = findViewById(R.id.search_button1);
        search_button2 = findViewById(R.id.search_button2);
        search_button3 = findViewById(R.id.search_button3);
        connect_button1 = findViewById(R.id.connect_button1);
        connect_button2 = findViewById(R.id.connect_button2);
        connect_button3 = findViewById(R.id.connect_button3);
        discover_button1 = findViewById(R.id.discoverSvc_button1);
        discover_button2 = findViewById(R.id.discoverSvc_button2);
        discover_button3 = findViewById(R.id.discoverSvc_button3);
        disconnect_button1 = findViewById(R.id.disconnect_button1);
        disconnect_button2 = findViewById(R.id.disconnect_button2);
        disconnect_button3 = findViewById(R.id.disconnect_button3);
        led_switch = findViewById(R.id.led_switch);
        cap_switch = findViewById(R.id.capsense_switch);
        button0 = findViewById(R.id.button0);
        led_switch2 = findViewById(R.id.led_switch2);
        Toolbar toolbar=findViewById(R.id.toolbar);

        //Activity Toolbar'ını kendi Toolbar'ımız ayarlıyoruz
        setSupportActionBar(toolbar);


        // Hizmet ve bağlantı durumu değişkenini başlat
        mServiceConnected = false;
        mConnectState = false;

        //Android 6.0 (Marshmallow)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Android M Permission check 
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access ");
                builder.setMessage("Please grant location access so this app can detect devices.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                builder.show();
            }
        } // Android 6.0 (Marshmallow)

        /* Bu LED Açma / Kapama düğmesine dokunulduğunda çağrılacaktır */
        led_switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Anahtarın durumuna bağlı olarak LED'i açma veya kapatma
                mPSoCCapSenseLedService.writeLed2Characteristic(isChecked);
            }
        });
        led_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Anahtarın durumuna bağlı olarak LED'i açma veya kapatma
                mPSoCCapSenseLedService.writeLedCharacteristic(isChecked);
            }
        });
        /*button0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //button durumuna göre gösterme
                mPSoCCapSenseLedService.writeButtonCharacteristic(isChecked);
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(IslemlerActivity.this, PSoCCapSenseLedService.class);
                startActivity(intent);
            }
        });*/




        /* Bu, CapSense Bildir Açık / Kapalı anahtarına dokunulduğunda çağrılacaktır */
        button0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPSoCCapSenseLedService.writeButtonCharacteristic(isChecked);

                byte[] value = new byte[1];
                if (isChecked) {value[0] = (byte) (1 & 0xFF);
                button0.setEnabled(true);
                } else {
                    value[0] = (byte) (0 & 0xFF);
                    button0.setEnabled(false);
                }


                }


        });
        cap_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Anahtarın durumuna göre CapSense Bildirimlerini açma / kapatma
                mPSoCCapSenseLedService.writeCapSenseNotification(isChecked);

                CapSenseNotifyState = isChecked;  // CapSense bildirim durumunu takip etme
                if(isChecked) { // Bildirimler artık yayında, metnin "No Touch" ifadesi var
                    mCapsenseValue.setText("touch");
                } else { // Bildirimler artık kapalı olduğundan metinde "Notify Off"
                    mCapsenseValue.setText(R.string.NotifyOff);
                }
            }
        });

    }

    // Android 6.0 (Marshmallow)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Permission for 6.0:", "Coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                }
            }
        }
    } //bitişi

    @Override
    protected void onResume() {
        super.onResume();
        // Yayın alıcısını kaydedin.
        // Bu, ana etkinliğin PSoCCapSenseLedService'den aradığı iletileri belirledi
        final IntentFilter filter = new IntentFilter();
        filter.addAction(PSoCCapSenseLedService.ACTION_BLESCAN_CALLBACK);
        filter.addAction(PSoCCapSenseLedService.ACTION_CONNECTED);
        filter.addAction(PSoCCapSenseLedService.ACTION_DISCONNECTED);
        filter.addAction(PSoCCapSenseLedService.ACTION_SERVICES_DISCOVERED);
        filter.addAction(PSoCCapSenseLedService.ACTION_DATA_RECEIVED);
        registerReceiver(mBleUpdateReceiver, filter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Kullanıcı Bluetooth'u etkinleştirmemeyi seçti.
        if (requestCode == REQUEST_ENABLE_BLE && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mBleUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Etkinlik sona erdiğinde hizmeti kapatın ve bağlantısını kesin
        if(mPSoCCapSenseLedService!=null)
        {
            mPSoCCapSenseLedService.close();
        }
        if(mServiceConnected)
        {
            unbindService(mServiceConnection);
        }
        mPSoCCapSenseLedService = null;

        mServiceConnected = false;
    }

    /**
     * Bu yöntem, bluetooth başlat düğmesini kullanır
     *
     * @param view
     */
    public void startBluetooth(View view) {

        // BLE servisini ve adaptörünü bulun
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();

        // Cihazda Bluetooth özelliğinin etkinleştirildiğinden emin olur. Bluetooth şu anda etkin değilse,
        // kullanıcıdan etkinleştirmek için izin vermesini isteyen bir iletişim kutusu görüntülemek amacıyla bir niyet başlatın.
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLE);
        }

        // BLE Hizmetini Başlat
        Log.d(TAG, "Starting BLE Service");
        Intent gattServiceIntent = new Intent(this, PSoCCapSenseLedService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        // Başlat düğmesini devre dışı bırakıldı ve arama düğmesini açıldı
        start_button.setEnabled(false);
        search_button1.setEnabled(true);
        search_button2.setEnabled(true);
        search_button3.setEnabled(true);
        Log.d(TAG, "Bluetooth is Enabled");
    }

    /**
     * Bu yöntem Cihaz Ara düğmesini işler
     *
     * @param view
     */
    public void searchBluetooth(View view) {
        if(mServiceConnected) {
            mPSoCCapSenseLedService.scan();
        }

        /*Bundan sonra, bir cihazın bulunduğunu algılamak için tarama geri aramasını bekleriz*/
        /* Geri arama mGattUpdateReceiver tarafından alınan bir mesaj yayınlar */
    }

    /**
     * Bu yöntem Cihaza Bağlan düğmesini işler
     *
     * @param view
     */
    public void connectBluetooth(View view) {
        mPSoCCapSenseLedService.connect();

        /* Bundan sonra, ağ geçidi geri aramasının cihazın bağlı olduğunu bildirmesini bekleriz*/
        /*Bu olay mGattUpdateReceiver tarafından alınan bir mesaj yayınlar */
    }

    /**
     * Bu yöntem, Hizmetleri ve Özellikleri Keşfet düğmesini kullanır
     *
     * @param view
     */
    public void discoverServices(View view) {
        /* Bu hem hizmetleri hem de özellikleri keşfedecek*/
        mPSoCCapSenseLedService.discoverServices();

        /* Bundan sonra gatt geri aramasının hizmetleri ve özellikleri bildirmesini bekliyoruz */
        /*Bu olay mGattUpdateReceiver tarafından alınan bir mesaj yayınlar */
    }

    /**
     * bağlantı koparma methodu
     *
     * @param view
     */
    public void Disconnect(View view) {
        mPSoCCapSenseLedService.disconnect();

        /*Bundan sonra ağ geçidinin cihazın bağlantısının kesildiğini bildirmesini bekleriz*/
        /*Bu olay mGattUpdateReceiver tarafından alınan bir mesaj yayınlar */
    }

    //Toolbar menüsü
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            //anasayfa seçilince anasayfa activity açılması
            case R.id.anasayfamenu:
                startActivity(new Intent(IslemlerActivity.this, AnasayfaActivity.class));
                return true;


            //fabeacon seçilince fabeacon activity açılması
            case R.id.fabeaconmenu:
                startActivity(new Intent(IslemlerActivity.this, FABeaconActivity.class));
                return true;


            //ihbar seçilince ihbar butonunun bulunduğu anasayfa activity açılması
            case R.id.ihbarmenu:
                startActivity(new Intent(IslemlerActivity.this, AnasayfaActivity.class));
                return true;


            //hakkında seçilince hakkında activity açılması
            case R.id.hakkinda:
                startActivity(new Intent(IslemlerActivity.this, HakkindaActivity.class));
                return true;
        }
        return false;
    }

    /**
     * BLE etkinlik yayınları için dinleyici
     */
    private final BroadcastReceiver mBleUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            switch (action) {
                case PSoCCapSenseLedService.ACTION_BLESCAN_CALLBACK:
                    // Arama düğmesini devre dışı bırak ve bağlan düğmesini etkinleştir
                    search_button1.setEnabled(false);
                    search_button2.setEnabled(false);
                    search_button3.setEnabled(false);
                    connect_button1.setEnabled(true);
                    connect_button2.setEnabled(true);
                    connect_button3.setEnabled(true);
                    break;

                case PSoCCapSenseLedService.ACTION_CONNECTED:
                    /*bir GATT_CONNECTED alıyoruz */
                    /* Capsense bildirimleri gönderirken yapılacak işlem */
                    if (!mConnectState) {
                        // Bağlan düğmesini devre dışı bırakın,
                        // keşif hizmetlerini ve bağlantı kesme düğmelerini etkinleştirin
                        connect_button1.setEnabled(false);
                        connect_button2.setEnabled(false);
                        connect_button3.setEnabled(false);
                        discover_button1.setEnabled(true);
                        discover_button2.setEnabled(true);
                        discover_button3.setEnabled(true);
                        disconnect_button1.setEnabled(true);
                        disconnect_button2.setEnabled(true);
                        disconnect_button3.setEnabled(true);
                        mConnectState = true;
                        Log.d(TAG, "Connected to Device");
                    }
                    break;
                case PSoCCapSenseLedService.ACTION_DISCONNECTED:
                    // Bağlantıyı kes, svc'yi keşfet, char düğmesini keşfet ve arama düğmesini etkinleştir
                    disconnect_button1.setEnabled(false);
                    disconnect_button2.setEnabled(false);
                    disconnect_button3.setEnabled(false);
                    discover_button1.setEnabled(false);
                    discover_button2.setEnabled(false);
                    discover_button3.setEnabled(false);
                    search_button1.setEnabled(true);
                    search_button2.setEnabled(true);
                    search_button3.setEnabled(true);;
                    // LED ve CapSense anahtarlarını kapatma ve devre dışı bırakma
                    led_switch.setChecked(false);
                    led_switch.setEnabled(false);
                    cap_switch.setChecked(false);
                    cap_switch.setEnabled(false);
                    button0.setChecked(false);
                    button0.setEnabled(false);
                    led_switch2.setChecked(false);
                    led_switch2.setEnabled(false);


                    mConnectState = false;
                    Log.d(TAG, "Disconnected");
                    break;
                case PSoCCapSenseLedService.ACTION_SERVICES_DISCOVERED:
                    // Hizmetleri keşfet düğmesini devre dışı bırakma
                    discover_button1.setEnabled(false);
                    discover_button2.setEnabled(false);
                    discover_button3.setEnabled(false);
                    // LED ve CapSense anahtarlarını etkinleştirme
                    led_switch.setEnabled(true);
                    led_switch2.setEnabled(true);
                    cap_switch.setEnabled(true);
                    button0.setEnabled(true);


                    Log.d(TAG, "Services Discovered");
                    break;
                case PSoCCapSenseLedService.ACTION_DATA_RECEIVED:
                    // Bu, bir bildirim veya okuma tamamlandıktan sonra çağrılır
                    // LED anahtar ayarını kontrol edin
                    if(mPSoCCapSenseLedService.getLedSwitchState()){
                        led_switch.setChecked(true);

                    } else {
                        led_switch.setChecked(false);
                    }
                    if(mPSoCCapSenseLedService.getLed2SwitchState()){
                        led_switch2.setChecked(true);


                    } else {
                        led_switch2.setChecked(false);

                    }

                    if (mPSoCCapSenseLedService.getButtonSwitchState()){

                        button0.setChecked(true);
                    }else{

                        button0.setChecked(false);
                    }


                    // CapSense Slider Değerini Alın
                    String CapSensePos = mPSoCCapSenseLedService.getCapSenseValue();
                    if (CapSensePos.equals("-1")) {  // Dokunmadan -1 olan 0xFFFF döndürür
                        if(!CapSenseNotifyState) { // Bildirimler kapalı
                            mCapsenseValue.setText(R.string.NotifyOff);
                        } else { // Bildirimler açık ancak kaydırıcıda parmak yok
                            mCapsenseValue.setText("NoTouch");
                        }
                    } else { // Geçerli CapSense değeri döndürüldü
                        mCapsenseValue.setText(CapSensePos);
                    }
                default:
                   /* button0.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mPSoCCapSenseLedService.getButtonSwitchState()){
                                switch (v.getId()){
                                    case R.id.button0:
                                        button0.setEnabled(true);
                                        break;
                                }

                            }else{
                                button0.setEnabled(false);

                            }
                        }
                    });*/
                    break;

            }
        }
    };
}
