
package com.fa.b;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGattCharacteristic;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * GATT veritabanı ile BLE veri bağlantısını yönetme servisi.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP) // lollipop sürüm desteklemesi ve  scan APIs
public class PSoCCapSenseLedService extends Service {
    private final static String TAG = PSoCCapSenseLedService.class.getSimpleName();

    // Etkileşimde bulunulacak ve gereken Bluetooth nesneleri
    private static BluetoothManager mBluetoothManager;
    private static BluetoothAdapter mBluetoothAdapter;
    private static BluetoothLeScanner mLEScanner;
    private static BluetoothDevice mLeDevice;
    private static BluetoothGatt mBluetoothGatt;

    private HashMap<String, BluetoothGattCharacteristic> mGattCharacteristics = new HashMap<>();

    // Okumamız / yazmamız gereken Bluetooth özellikleri
    private static BluetoothGattCharacteristic mLedCharacterisitc;
    private static BluetoothGattCharacteristic mLed2Characteristic;
    private static BluetoothGattCharacteristic mButtonCharacteristic;
    private static BluetoothGattCharacteristic mCapsenseCharacteristic;
    private static BluetoothGattCharacteristic mCapsense2Characteristic;
    private static BluetoothGattDescriptor mCapSenseCccd;
    private static BluetoothGattDescriptor mCapSense2Cccd;

    // servis UUID leri
    private final static String baseUUID = "F0001110-0451-4000-B000-000000000000";
    private final static String capsenseLedServiceUUID = baseUUID + "0";
    public final static String ledCharacteristicUUID ="F0001111-0451-4000-B000-000000000000";
    public final static  String led2CharacteristicUUID ="F0001112-0451-4000-B000-000000000000";
    public final static String capsenseCharacteristicUUID ="F0001111-0451-4000-B000-000000000000";
    public final static String capsense2CharacteristicUUID ="F0001112-0451-4000-B000-000000000000";
    private final static String CccdUUID = "00002902-0000-1000-8000-00805f9b34fb";
    public static String BUTTON_SERVICE = "F0001120-0451-4000-B000-000000000000";
    public static String BUTTON0_STATE = "F0001121-0451-4000-B000-000000000000";

    // LED durumunu ve CapSense Değerini takip ve Data okuma değişkenleri
    private static boolean mLedSwitchState = false;
    private static boolean mLed2SwitchState = false ;
    private static boolean mButtonSwitchState =false;
    private static String mCapSenseValue = "-1"; // This is the No Touch value (0xFFFF)
    private static String mCapSense2Value = "-1";

    // Ana etkinliğe yapılan yayınlar sırasında kullanılan eylemler
    public final static String ACTION_BLESCAN_CALLBACK =
            "ACTION_BLESCAN_CALLBACK";
    public final static String ACTION_CONNECTED =
            "ACTION_CONNECTED";
    public final static String ACTION_DISCONNECTED =
            "ACTION_DISCONNECTED";
    public final static String ACTION_SERVICES_DISCOVERED =
            "ACTION_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_RECEIVED =
            "ACTION_DATA_RECEIVED";

    public PSoCCapSenseLedService() {
    }



    public class LocalBinder extends Binder {
        public PSoCCapSenseLedService getService() {
            return PSoCCapSenseLedService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // Kaynakları boşaltmak için hizmeti açtığımızda BLE kapatma yöntemi çağrılır.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Yerel Bluetooth adaptörüne bir referans başlatır.
     *
     * @return Başlatma başarılı olursa true döndür.
     */
    public boolean initialize() {

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");//BLE BAŞLATILAMADI
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");//BLE ADAPTER alınamıyor
            return false;
        }

        return true;
    }

    /**
     * Aradığınız hizmeti destekleyen BLE cihazlarını tarar
     */
    public void scan() {
        /* Cihazları tarayın ve istediğimiz hizmete sahip olanı arayın */
        UUID capsenseLedService = UUID.fromString(capsenseLedServiceUUID);
        UUID[] capsenseLedServiceArray = {capsenseLedService};

        UUID capsenseLed2Service =UUID.fromString(capsenseLedServiceUUID);
        UUID[]capsenseLed2ServiceArray = {capsenseLed2Service};


        UUID buttonservice = UUID.fromString(BUTTON0_STATE);
        UUID[] buttonserviceArray = {buttonservice};

        // lollipop versiyon desteği
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //inceleme
            mBluetoothAdapter.startLeScan(capsenseLedServiceArray, mLeScanCallback);
        } else { // BLE tarama LOLLIPOP
            ScanSettings settings;
            List<ScanFilter> filters;
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<>();
            // Filtreleme sadece ARAÇ UUID si üzerine tarama
            ParcelUuid PUuid = new ParcelUuid(capsenseLedService);
            ScanFilter filter = new ScanFilter.Builder().setServiceUuid(PUuid).build();
            filters.add(filter);
            mLEScanner.startScan(filters, settings, mScanCallback);
        }
        // lollipop versiyon desteği
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //inceleme
            mBluetoothAdapter.startLeScan(capsenseLed2ServiceArray, mLeScanCallback);

        } else { // BLE tarama LOLLIPOP
            ScanSettings settings;
            List<ScanFilter> filters;
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<>();
            // Filtreleme sadece ARAÇ UUID si üzerine tarama
            ParcelUuid PUuid = new ParcelUuid(capsenseLed2Service);
            ScanFilter filter = new ScanFilter.Builder().setServiceUuid(PUuid).build();
            filters.add(filter);
            mLEScanner.startScan(filters, settings, mScanCallback);
        }
        // lollipop versiyon desteği
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //inceleme
            mBluetoothAdapter.startLeScan(buttonserviceArray,mLeScanCallback);
        } else { // BLE tarama LOLLIPOP
            ScanSettings settings;
            List<ScanFilter> filters;
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<>();
            // Filitreleme sadece ARAÇ UUID si üzerine tarama
            ParcelUuid PUuid = new ParcelUuid(buttonservice);
            ScanFilter filter = new ScanFilter.Builder().setServiceUuid(PUuid).build();
            filters.add(filter);
            mLEScanner.startScan(filters, settings, mScanCallback);
        }

    }

    /**
     * Bluetooth LE cihazında barındırılan GATT sunucusuna bağlanır.
     *
     * @return Bağlantı başarıyla başlatılırsa true değerini döndürün.
     * Bağlantı sonucu, eşzamansız olarak
     *
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean connect() {
        if (mBluetoothAdapter == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");//BLT başlatılamadı
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");//GAtt kullanımı aktifleştirildi
            return mBluetoothGatt.connect();
        }

        // Cihaza doğrudan bağlanmak istiyoruz, bu yüzden autoConnect'i ayarlıyoruz
        // parametresini false olarak ayarladık mümkünse true olacak.
        mBluetoothGatt = mLeDevice.connectGatt(this, false, mGattCallback);
        Log.d(TAG, "Trying to create a new connection.");
        return true;
    }

    /**
     *  Bağlı cihazda servis bulmayı çalıştırır.
     */
    public void discoverServices() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");//bağlantısızlık durumunda tekrar döndür
            return;
        }
        mBluetoothGatt.discoverServices();
    }

    /**
     * Mevcut bir bağlantının bağlantısını keser veya bekleyen bir bağlantıyı iptal eder. Bağlantı kesilmesi sonucu
     * zaman gözetmeden call back yap
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /**
     * Belirli bir BLE cihazını kullandıktan sonra,
     * uygulama kaynakların düzgün bir şekilde serbest bırakıldığından emin olmak için bu yöntemi çağırmalıdır.
     *
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    /**
     * LED'in durumunu cihazdan okumak için kullanılır
     */
    public void readLed2Characteristic(){
        if (mBluetoothAdapter == null || mBluetoothGatt ==null){
            Log.w(TAG,"not");
            return;
        }
        mBluetoothGatt.readCharacteristic(mLed2Characteristic);
    }
    public void writeLed2Characteristic(boolean value){
        byte[] byteVal = new byte[1];
        if (value) {
            byteVal[0] = (byte) (1);
        } else {
            byteVal[0] = (byte) (0);
        }
        Log.i(TAG, "LED 2" + value);
        mLed2SwitchState = value;
        mLed2Characteristic.setValue(byteVal);
        mBluetoothGatt.writeCharacteristic(mLed2Characteristic);
    }
    public void readLedCharacteristic() {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(mLedCharacterisitc);

    }

    /**
     * Led durumu 1 0 olmaya bakıyor
     * @param value  LED aç (1) kapa (0)
     */
    public void writeLedCharacteristic(boolean value) {
        byte[] byteVal = new byte[1];
        if (value) {
            byteVal[0] = (byte) (1);
        } else {
            byteVal[0] = (byte) (0);
        }
        Log.i(TAG, "LED " + value);
        mLedSwitchState = value;
        mLedCharacterisitc.setValue(byteVal);
        mBluetoothGatt.writeCharacteristic(mLedCharacterisitc);
    }

    public void readButtonCharacteristic(boolean b) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        if((mButtonCharacteristic.getPermissions() == 1)) {
            mBluetoothGatt.readCharacteristic(mButtonCharacteristic);
        }

    }
    public void writeButtonCharacteristic(boolean value){
        byte[] byteVal = new byte[1];
        if (value) {
            byteVal[0] = (byte) (1);
        } else{
            byteVal [0] = (byte) (0);
        }
        Log.i(TAG,"BUTTON" + value);
        mButtonSwitchState=value;
        mButtonCharacteristic.setValue(byteVal);
        mBluetoothGatt.writeCharacteristic(mButtonCharacteristic);

    }

    /**
     * Bu yöntem, CapSense kaydırıcısı için bildirimleri etkinleştirir veya devre dışı bırakır
     *
     * @param value
     */
    public void writeCapSenseNotification(boolean value) {
        // Set notifications locally in the CCCD
        mBluetoothGatt.setCharacteristicNotification(mCapsenseCharacteristic, value);
        byte[] byteVal = new byte[1];
        if (value) {
            byteVal[0] = 1;
        } else {
            byteVal[0] = 0;
        }
        // Write Notification value to the device
        Log.i(TAG, "CapSense Notification " + value);
        mCapSenseCccd.setValue(byteVal);
        mBluetoothGatt.writeDescriptor(mCapSenseCccd);
    }

    /**
     * led ve button durumunun gözlemlenmesi
     *
     * @return duruma göre switch durumunun düzenlenmesi
     */
    public boolean getLedSwitchState() {
        return mLedSwitchState;
    }

    public boolean getLed2SwitchState() {return  mLed2SwitchState;}

    public boolean getButtonSwitchState(){
        return mButtonSwitchState;
    }


    public String getCapSenseValue() {
        return mCapSenseValue;
    }
    public String getCapSense2Value() {
        return mCapSense2Value;
    }


    /**
     * Aradığımız hizmetin bulunduğu bir cihaz bulduğunda cihazların geri aranması için geri aramayı uygular.
     *
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    mLeDevice = device;

                    mBluetoothAdapter.stopLeScan(mLeScanCallback); //bir cihaz bulunduğunda tarama durduruldu
                    // daha hızlı işlem yapılır ve pil koruması sağlanır
                    broadcastUpdate(ACTION_BLESCAN_CALLBACK); // Ana etkinliğe bir cihazın bulunduğunu söyleyin
                }
            };

    /**
     * Aradığımız hizmetin bulunduğu bir cihaz bulduğunda cihazların geri aranması için geri aramayı uygular.
     *
     * LOLİPOP İÇİNDE AYNI İŞLEM YAPILIR
     */
    private final ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            mLeDevice = result.getDevice();
            mLEScanner.stopScan(mScanCallback);
            broadcastUpdate(ACTION_BLESCAN_CALLBACK);
        }
    };


    /**
     * Uygulamanın önem verdiği GATT etkinlikleri için geri arama yöntemleri uygular.
     * Örneğin, bağlantı değişikliği ve keşfedilen hizmetler.
     */
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                broadcastUpdate(ACTION_CONNECTED);
                Log.i(TAG, "Connected to GATT server.");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastUpdate(ACTION_DISCONNECTED);
            }
        }

        /**
         * Burası, bir servis keşfi tamamlandığında çağrılır.
         *
         * 1)İlgilendiğimiz karakteristikleri alır
         * 2) ardından ana faaliyet için bir güncelleme yayınlar.
         *
         * @param gatt
         * @param status
         */
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            // Sadece aradığımız hizmeti alın
            BluetoothGattService mService = gatt.getService(UUID.fromString(capsenseLedServiceUUID));
            BluetoothGattService mService2 = gatt.getService(UUID.fromString(BUTTON_SERVICE));
            /* İstediğiniz hizmetten özellikler edinin  */
            mLedCharacterisitc = mService.getCharacteristic(UUID.fromString(ledCharacteristicUUID));
            mLed2Characteristic = mService.getCharacteristic(UUID.fromString(led2CharacteristicUUID));
            mButtonCharacteristic = mService2.getCharacteristic(UUID.fromString(BUTTON0_STATE));
            mCapsenseCharacteristic = mService.getCharacteristic(UUID.fromString(capsenseCharacteristicUUID));
            mCapsense2Characteristic =mService.getCharacteristic(UUID.fromString(capsense2CharacteristicUUID));
            /*CapSense CCCD'sini edinin */
            mCapSenseCccd = mCapsenseCharacteristic.getDescriptor(UUID.fromString(CccdUUID));
            mCapSense2Cccd = mCapsense2Characteristic.getDescriptor(UUID.fromString(CccdUUID));

            //Led ve button durumu okuması
            readLedCharacteristic();
            readLed2Characteristic();
            readButtonCharacteristic(mButtonCharacteristic == null);


            // Hizmet / karakteristik / tanımlayıcı keşfinin yapıldığını yayınlayın
            broadcastUpdate(ACTION_SERVICES_DISCOVERED);
        }

        /**
         * okuma sonrası callback
         *
         * @param gatt  GATT veritabanı nesnesi
         * @param characteristic  GATT karakteristik okuma
         * @param status  status işlem
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {


            if (status == BluetoothGatt.GATT_SUCCESS) {
                // Okumanın LED durumu olduğunu doğrulayın
                String uuid = characteristic.getUuid().toString();
                //Bu durumda, uygulamanın yaptığı tek okuma LED durumudur.
                // Uygulamanın okumak için ek özellikleri varsa,
                // burada her biri üzerinde ayrı ayrı çalışmak için bir anahtar deyimi kullanabiliriz.
                if (uuid.equalsIgnoreCase(ledCharacteristicUUID)) {
                    final byte[] data = characteristic.getValue();
                    // LED anahtar durumu değişkenini, okunan karakteristik değere göre ayarlandı
                    mLedSwitchState = ((data[0] & 0xff) != 0x00);
                }
                if (uuid.equalsIgnoreCase(led2CharacteristicUUID)){
                    final byte[] data = characteristic.getValue();
                    mLed2SwitchState = ((data[0] & 0xff) != 0x00);
                }
                if (uuid.equalsIgnoreCase(BUTTON0_STATE)){
                    final byte[] data = characteristic.getValue();

                    mButtonSwitchState = ((data[0] & 0xff) != 0x00);

                }
                // Yeni etkinliğin mevcut olduğunu ana etkinliğe bildirin
                broadcastUpdate(ACTION_DATA_RECEIVED);
            }
        }

        /**
         * Bildirim kümesine sahip bir karakteristik değiştiğinde buna çağrılır.
         * Değişen verilerle ana faaliyet için bir güncelleme yayınlar.
         *
         * @param gatt
         * @param characteristic karekteristik yapı değişikliği
         */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {

            String uuid = characteristic.getUuid().toString();

            // Bu durumda, uygulamaların aldığı tek bildirim CapSense değeridir.
            // Uygulamanın ek bildirimleri varsa,
            // burada her biri üzerinde ayrı ayrı çalışmak için bir anahtar deyimi kullanabiliriz.
            if (uuid.equalsIgnoreCase(capsenseCharacteristicUUID)) {
                mCapSenseValue = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0).toString();
            }
            if (uuid.equalsIgnoreCase(capsense2CharacteristicUUID)){
                mCapSense2Value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0).toString();
             }

            //Yeni etkinliğin mevcut olduğunu ana etkinliğe bildirir
            broadcastUpdate(ACTION_DATA_RECEIVED);
        }
    }; // GATT olay geri arama yöntemlerinin sonu

    /**
     * Ana etkinlikte dinleyiciye bir yayın gönderir.
     *
     * @param action
     */
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }
    public void onDataRead(View view){
        if (mGattCharacteristics != null){
            //R/W
            final BluetoothGattCharacteristic characteristic = mGattCharacteristics.get("String char");
            if (characteristic == null){
                return;
            }
            if ((characteristic.getProperties() | BluetoothGattCharacteristic.PROPERTY_READ ) > 0){
                readButtonCharacteristic(mButtonCharacteristic == null);

            }

        }
    }


}