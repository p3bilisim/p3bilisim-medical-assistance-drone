package com.fa.b;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class AteslenmeZamanlariActivity extends AppCompatActivity {

    DatabaseHelper db;//Database İşlem sınıfı tanımı
    Button addAteslenmeButton;//Ataşleme ekleme butonu
    ListView ateslemeList;//Ataşlemelerin görüneceği liste
    ArrayList<String> listItem;//Liste itemlerini tutacak Array List
    ArrayAdapter adapter;// Array list için adapter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ateslenme_zamanlari);
        //Database İşlem sınıfı tanımı
        db=new DatabaseHelper(this);

        //Itemlerin tanımlanması
        listItem=new ArrayList<>();
        addAteslenmeButton=findViewById(R.id.addAteslenmeButton);
        ateslemeList=findViewById(R.id.ateslemeList);
        Toolbar toolbar=findViewById(R.id.toolbar);

        //Listenin Temizlenmesi
        listItem.clear();

        //Temizlenen listeye veri ekleme
        viewData();

        //Activity toolbarının dafault yerine kendi toolbarımız ayarlanması
        setSupportActionBar(toolbar);
    }

    //Toolbardaki seçenekler tuşuna basılınca menü çıkması
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    //Bakas bilişim yazısı seçilince anasayfa activity açılması
    public void anasayfa_buton(View view)
    {
        startActivity(new Intent(this, AnasayfaActivity.class));
    }

    //Toolbar menüsünde seçim yapılınca çalışacak kodlar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            //anasayfa seçilince anasayfa activity açılması
            case R.id.anasayfamenu:
                startActivity(new Intent(AteslenmeZamanlariActivity.this, AnasayfaActivity.class));
                return true;


            //işlemler seçilince işlemler activity açılması
            case R.id.islemlermenu:
                startActivity(new Intent(AteslenmeZamanlariActivity.this, IslemlerActivity.class));
                return true;


            //fabeacon seçilince fabeacon activity açılması
            case R.id.fabeaconmenu:
                startActivity(new Intent(AteslenmeZamanlariActivity.this, FABeaconActivity.class));
                return true;


            //ihbar seçilince ihbar butonunun bulunduğu anasayfa activity açılması
            case R.id.ihbarmenu:
                startActivity(new Intent(AteslenmeZamanlariActivity.this, AnasayfaActivity.class));
                return true;


            //hakkında seçilince hakkında activity açılması
            case R.id.hakkinda:
                startActivity(new Intent(AteslenmeZamanlariActivity.this, HakkindaActivity.class));
                return true;
        }
        return false;
    }

    //Listeye veri ekleme fonksiyonu
    private void viewData() {
        Cursor cursor =db.viewData();//Veritabanından verileri srayla alacak cursor

        //Veri var mı kontrol
        if(cursor.getCount()==0){
            Toast.makeText(this,"Gösterilecek veri yok",Toast.LENGTH_SHORT).show();
        }
        //veri varsa Array list e ekleme
        else{
            while (cursor.moveToNext()){
                listItem.add(cursor.getString(1));
            }
            //Arraylist i listview e uyarlayan adapter
            adapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,listItem);
            ateslemeList.setAdapter(adapter);
        }
    }

    //Ateşleme ekle butonu fonksiyonu
    public void addAteslenmeButton(View view)
    {
        AteslenmeZamanlariActivity.this.finish();
        startActivity(new Intent(AteslenmeZamanlariActivity.this, AteslenmeEkleActivity.class));
    }
    //Ateşlenme Grafiği Görüntüle butonu fonksiyonu
    public void ateslenmeGrafikButton(View view)
    {
        startActivity(new Intent(AteslenmeZamanlariActivity.this, AteslenmeGrafikActivity.class));
    }
    //Ateşlenme Verisi Sıfırla butonu fonksiyonu
    public void deleteAteslenmeButton(View view)
    {
        db.deleteData("ateslenme_zamani","*");
        ateslemeList.setAdapter(null);
    }
}
